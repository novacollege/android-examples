# Waarvoor is dit project voor

Dit is een kant en klaar AndroidStudio project (Android app).

Het project bevat belangrijke elementen om mee te beginnen als android developer!

Een aantal:

* Navigeren naar een ander scherm
* Data verwerking in de SQLite database van een Android device
* Het efficient tonen van data in lijsten (RecycleViews)
* Verbinden met een eigen RESTful API om data op te halen
* Data tonen en naar een detail scherm navigeren


# Hoe kan ik dit project starten

Door de code te downloaden via: [ZIP files](https://bitbucket.org/novacollege/android-examples/downloads/?tab=tags)

Eerst AndroidStudio starten en via het menu: `File` => `Open` (zoek de map)


## LET OP!

De android simulator dient natuurlijk verbinding te zoeken met jouw eigen webserver (Apache+PHP).
Het is dus belangrijk om te weten wat jouw ip-adres is van jouw webserver. Als je dit weet zal je dit in de code van dit androidproject moeten verwerken. Anders verbindt hij niet!
 
Het instellen van het ipadres moet in de class: `nl.novacollege.ictacademie.amo.android.androidexamples.api.APIClient` (file: APIClient.java)

Hieronder de betreffende code:

```java

package nl.novacollege.ictacademie.amo.android.androidexamples.api;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ivo on 27/03/2018.
 */

public class APIClient {


    private static Retrofit retrofit = null;

    public static Retrofit getClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl("http://[HIER HET JUISTE IP-ADRES!]/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        return retrofit;
    }


}


```



## RESTful API
Bij dit project hoort ook een RESTful API (PHP).

Dit omdat je dan echt zelf jouw api beheert en kunt veranderen.

_Zo ben je niet afhankelijk van het internet ;)_

Hier is de API te vinden:

* De instructies: [GIT repository](https://bitbucket.org/novacollege/android-test-api)
* Downloaden source code: [ZIP files](https://bitbucket.org/novacollege/android-test-api/downloads/?tab=tags)