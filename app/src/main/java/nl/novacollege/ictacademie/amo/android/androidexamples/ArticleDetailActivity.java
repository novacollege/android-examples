package nl.novacollege.ictacademie.amo.android.androidexamples;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import nl.novacollege.ictacademie.amo.android.androidexamples.api.APIClient;
import nl.novacollege.ictacademie.amo.android.androidexamples.api.APIInterface;
import nl.novacollege.ictacademie.amo.android.androidexamples.api.Article;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleDetailActivity extends AppCompatActivity {

    private static final String TAG = ArticleDetailActivity.class.getName();
    private CoordinatorLayout coordinatorLayout;
    private APIInterface apiInterface;
    private Context context;
    private ProgressBar progress;
    private TextView textViewArticleName;
    private TextView textViewArticleAuthor;
    private TextView textViewArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);

        context = this;

        progress = findViewById(R.id.progress_bar);
        coordinatorLayout = findViewById(R.id.main_content);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        textViewArticleName = findViewById(R.id.textview_article_name);
        textViewArticleAuthor = findViewById(R.id.textview_article_author);
        textViewArticle = findViewById(R.id.textview_article);
        textViewArticle.setMovementMethod(new ScrollingMovementMethod());

    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        long article = bundle.getLong("articleId", -1);

        if (article >= 0) {
            /**
             GET List Resources
             **/
            progress.setVisibility(View.VISIBLE);
            textViewArticleName.setVisibility(View.GONE);
            textViewArticleAuthor.setVisibility(View.GONE);
            textViewArticle.setVisibility(View.GONE);


            Call<Article> call = apiInterface.getArticle(article);
            call.enqueue(new Callback<Article>() {
                @Override
                public void onResponse(Call<Article> call, Response<Article> response) {
                    Log.d(TAG, response.code() + "");
                    Article article = response.body();

                    progress.setVisibility(View.GONE);
                    textViewArticleName.setVisibility(View.VISIBLE);
                    textViewArticleAuthor.setVisibility(View.VISIBLE);
                    textViewArticle.setVisibility(View.VISIBLE);
                    textViewArticleName.setText(article.getName());
                    CharSequence author = context.getResources().getString(R.string.txt_author, article.getAuthor());
                    textViewArticleAuthor.setText(author);
                    textViewArticle.setText(article.getText());
                }

                @Override
                public void onFailure(Call<Article> call, Throwable t) {
                    call.cancel();

                    textViewArticleName.setText("");
                    textViewArticleAuthor.setText("");
                    textViewArticle.setText("");

                    progress.setVisibility(View.GONE);

                    // Snackbar to show a message
                    CharSequence text = getResources().getString(R.string.txt_error, t.getLocalizedMessage());
                    Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
                    snackbar.show();

                }


            });

        } else {
            // Snackbar to show a message
            CharSequence text = getResources().getString(R.string.txt_just_no_data);
            Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
}
