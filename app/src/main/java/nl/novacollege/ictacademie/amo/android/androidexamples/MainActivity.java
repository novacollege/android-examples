package nl.novacollege.ictacademie.amo.android.androidexamples;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate triggered!!");

        Button btnBoven = (Button) findViewById(R.id.button_boven);
        Button btnBeneden = (Button) findViewById(R.id.button_beneden);

        btnBoven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "btnBoven Clicked!");
                navigateToScreen1();
            }
        });

        btnBeneden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "btnBeneden Clicked!");
                navigateToScreen2();
            }
        });
    }

    /**
     * Navigeer naar scherm1 (nog zonder datauitwisseling)
     */
    private void navigateToScreen1() {
        Intent intent = new Intent(this, Screen1Activity.class);
        startActivity(intent);
    }

    /**
     * Navigeer naar scherm2 (nog zonder datauitwisseling)
     */
    private void navigateToScreen2() {
        Intent intent = new Intent(this, Screen2Activity.class);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart triggered!!");
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        Log.d(TAG, "onRestart triggered!!");
    }


    @Override
    protected void onPause() {
        super.onPause();

        Log.d(TAG, "onPause triggered!!");
    }
}
