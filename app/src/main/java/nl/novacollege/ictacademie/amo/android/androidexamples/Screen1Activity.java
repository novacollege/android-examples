package nl.novacollege.ictacademie.amo.android.androidexamples;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import nl.novacollege.ictacademie.amo.android.androidexamples.database.model.ToDoItem;
import nl.novacollege.ictacademie.amo.android.androidexamples.database.sqlite.ToDoListDatabase;
import nl.novacollege.ictacademie.amo.android.androidexamples.widget.ToDoRecycleViewAdapter;


public class Screen1Activity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;
    private ToDoListDatabase database;
    private EditText editTextName;
    private EditText editTextDescription;

    private ToDoRecycleViewAdapter adapter;


    /**
     * Dedicated method to add an item to the to-do database.
     * - Fetches name and description from the corresponding fields
     * - Creates a ToDoItem instance and delegates this to the database instance which in turn ensures it's added in the SQLite database
     * - When it's stored, the input fields are emptied
     * - As an example a small notification is presented
     */
    private void addToDoAndClearEditText() {

        String name = editTextName.getText().toString().trim();
        String description = editTextDescription.getText().toString().trim();
        if (name.length() > 0) {
            ToDoItem toDoItem = new ToDoItem();
            toDoItem.name = name;
            toDoItem.description = description;
            long rowId = database.addToDo(toDoItem);

            // Notify adapter which will refresh the data in the recycler view
            adapter.notifyWithNewData(database.findAllToDo());

            // Cleanup fields
            editTextName.getText().clear();
            editTextDescription.getText().clear();

            // Snackbar to show a message
            CharSequence text = getResources().getString(R.string.txt_toast_added_todo, name);
            Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_SHORT);
            snackbar.show();

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen1);

        Context context = this;

        // Fetch an instance of our own ToDoListDatabase class
        database = new ToDoListDatabase(context);

        // Create a new ToDoRecycleView Adapter, which needs context and a database cursor to fetch data
        adapter = new ToDoRecycleViewAdapter(context, database.findAllToDo());

        // Fetch views from layout
        coordinatorLayout = findViewById(R.id.main_content);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_todo);
        Button btnAddToDo = findViewById(R.id.button_add_todo);
        editTextName = findViewById(R.id.edit_text_name);
        editTextDescription = findViewById(R.id.edit_text_description);

        // Attach a layout manager and the data adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);


        // Listen to "add a to do" button click event
        btnAddToDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToDoAndClearEditText();
            }
        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                database.removeToDo((long) viewHolder.itemView.getTag());
                adapter.notifyWithNewData(database.findAllToDo());

                Context context = getApplicationContext();
                CharSequence text = "Taak verwijderd";
                int duration = Toast.LENGTH_SHORT;

                // Show a small message to notify one is removed
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }
        });
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    protected void onDestroy() {
        database.close();
        super.onDestroy();
    }
}
