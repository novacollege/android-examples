package nl.novacollege.ictacademie.amo.android.androidexamples;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import nl.novacollege.ictacademie.amo.android.androidexamples.api.APIClient;
import nl.novacollege.ictacademie.amo.android.androidexamples.api.APIInterface;
import nl.novacollege.ictacademie.amo.android.androidexamples.api.Article;
import nl.novacollege.ictacademie.amo.android.androidexamples.widget.ArticleRecycleViewAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Screen2Activity extends AppCompatActivity {

    private static final String TAG = Screen2Activity.class.getName();

    private CoordinatorLayout coordinatorLayout;
    private ProgressBar progress;
    private APIInterface apiInterface;
    private ArticleRecycleViewAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        Context context = this;

        progress = findViewById(R.id.progress_bar);
        coordinatorLayout = findViewById(R.id.main_content);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        // Create a new ToDoRecycleView Adapter, which needs context and a database cursor to fetch data
        adapter = new ArticleRecycleViewAdapter(context);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_articles);

        // Attach a layout manager and the data adapter
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(adapter);


    }


    @Override
    protected void onStart() {
        super.onStart();

        progress.setVisibility(View.VISIBLE);

        /**
         GET List Resources
         **/
        Call<List<Article>> call = apiInterface.listArticles();
        call.enqueue(new Callback<List<Article>>() {
            @Override
            public void onResponse(Call<List<Article>> call, Response<List<Article>> response) {
                Log.d(TAG, String.valueOf(response.code()));

                progress.setVisibility(View.GONE);

                List<Article> articles = response.body();

                // Notify adapter which will refresh the data in the recycler view
                adapter.notifyWithNewData(articles);

            }

            @Override
            public void onFailure(Call<List<Article>> call, Throwable t) {
                call.cancel();
                adapter.notifyWithNewData(new ArrayList<Article>());

                progress.setVisibility(View.GONE);

                // Snackbar to show a message
                CharSequence text = getResources().getString(R.string.txt_error, t.getLocalizedMessage());
                Snackbar snackbar = Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG);
                snackbar.show();


            }


        });
    }
}
