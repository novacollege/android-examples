package nl.novacollege.ictacademie.amo.android.androidexamples.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by ivo on 27/03/2018.
 */

public interface APIInterface {

    @GET("articles")
    Call<List<Article>> listArticles();

    @GET("articles/{id}")
    Call<Article> getArticle(@Path("id") long id);


}
