package nl.novacollege.ictacademie.amo.android.androidexamples.database.sqlite;

import android.provider.BaseColumns;

/**
 * Created by ivo on 19/03/2018.
 */

public final class ToDoListContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ToDoListContract() {

    }

    /* Inner class that defines the table contents */
    public static class ToDoListEntry implements BaseColumns {
        public static final String TABLE_NAME = "ToDoListItem";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_DONE = "done";
        public static final String COLUMN_CREATED_AT = "createdAt";
    }


}
