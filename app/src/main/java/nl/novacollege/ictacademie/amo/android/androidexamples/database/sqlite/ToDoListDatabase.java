package nl.novacollege.ictacademie.amo.android.androidexamples.database.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import nl.novacollege.ictacademie.amo.android.androidexamples.database.model.ToDoItem;


/**
 * Created by ivo on 26/03/2018.
 */

public class ToDoListDatabase {

    private ToDoListSQLHelper toDoListSQLHelper;
    private SQLiteDatabase database;
    private Context context;


    public ToDoListDatabase(Context context) {
        this.context = context;

        toDoListSQLHelper = new ToDoListSQLHelper(context);
    }


    public void close() {
        toDoListSQLHelper.close();
    }


    public long addToDo(ToDoItem toDoItem) {
        // Gets the data repository in write mode
        SQLiteDatabase db = toDoListSQLHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ToDoListContract.ToDoListEntry.COLUMN_NAME, toDoItem.name);
        values.put(ToDoListContract.ToDoListEntry.COLUMN_DESCRIPTION, toDoItem.description);
        values.put(ToDoListContract.ToDoListEntry.COLUMN_DONE, toDoItem.done);
        values.put(ToDoListContract.ToDoListEntry.COLUMN_CREATED_AT, ToDoItem.getCurrentDateTime());

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(ToDoListContract.ToDoListEntry.TABLE_NAME, null, values);

        return newRowId;

    }


    public Cursor findAllToDo() {

        SQLiteDatabase db = toDoListSQLHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                ToDoListContract.ToDoListEntry.COLUMN_NAME,
                ToDoListContract.ToDoListEntry.COLUMN_DESCRIPTION,
                ToDoListContract.ToDoListEntry.COLUMN_DONE,
                ToDoListContract.ToDoListEntry.COLUMN_CREATED_AT
        };


        String selection = ToDoListContract.ToDoListEntry.COLUMN_NAME + " IS NOT NULL";

        // How you want the results sorted in the resulting Cursor
        String sortOrder =
                ToDoListContract.ToDoListEntry.COLUMN_CREATED_AT + " DESC";

        Cursor cursor = db.query(
                ToDoListContract.ToDoListEntry.TABLE_NAME,
                projection,
                selection,
                null,
                null,
                null,
                sortOrder
        );

        return cursor;
    }

    public void removeToDo(long id) {
        SQLiteDatabase db = toDoListSQLHelper.getWritableDatabase();
        String[] whereArgument = new String[]{String.valueOf(id)};
        db.delete(ToDoListContract.ToDoListEntry.TABLE_NAME, ToDoListContract.ToDoListEntry._ID + "=?", whereArgument);
    }

}
