package nl.novacollege.ictacademie.amo.android.androidexamples.database.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ivo on 19/03/2018.
 */

public class ToDoListSQLHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    // Database info:
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "amo-android-opensqlitedatabase.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ToDoListContract.ToDoListEntry.TABLE_NAME + " (" +
                    ToDoListContract.ToDoListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    ToDoListContract.ToDoListEntry.COLUMN_NAME + " TEXT," +
                    ToDoListContract.ToDoListEntry.COLUMN_DESCRIPTION + " TEXT," +
                    ToDoListContract.ToDoListEntry.COLUMN_DONE + " INTEGER," +
                    ToDoListContract.ToDoListEntry.COLUMN_CREATED_AT + " TEXT" +
                    ");";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + ToDoListContract.ToDoListEntry.TABLE_NAME;

    public ToDoListSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over

        Log.d(ToDoListSQLHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");

        sqLiteDatabase.execSQL(SQL_DELETE_ENTRIES);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        onUpgrade(sqLiteDatabase, oldVersion, newVersion);
    }
}
