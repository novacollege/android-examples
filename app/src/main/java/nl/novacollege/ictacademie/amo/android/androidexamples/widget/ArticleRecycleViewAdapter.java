package nl.novacollege.ictacademie.amo.android.androidexamples.widget;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import nl.novacollege.ictacademie.amo.android.androidexamples.R;
import nl.novacollege.ictacademie.amo.android.androidexamples.api.Article;

/**
 * Created by ivo on 28/03/2018.
 */

public class ArticleRecycleViewAdapter extends RecyclerView.Adapter<ArticleViewHolder> {

    private List<Article> articles = new ArrayList<>();
    private Context context;


    public ArticleRecycleViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        View view = layoutInflater.inflate(R.layout.layout_article, parent, false);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticleViewHolder holder, int position) {

        Article article = articles.get(position);
        holder.articleId = article.getId();
        holder.textViewName.setText(article.getName());
        CharSequence author = context.getResources().getString(R.string.txt_author, article.getAuthor());
        holder.textViewAuthor.setText(author);
        holder.textViewCreatedAt.setText("");

    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (null != articles) {
            count = articles.size();
        }
        return count;
    }


    public void notifyWithNewData(List<Article> articles) {

        // Attach new articles to adapter property and notify
        this.articles = articles;
        notifyDataSetChanged();

    }
}
