package nl.novacollege.ictacademie.amo.android.androidexamples.widget;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nl.novacollege.ictacademie.amo.android.androidexamples.ArticleDetailActivity;
import nl.novacollege.ictacademie.amo.android.androidexamples.R;

/**
 * Created by ivo on 28/03/2018.
 */

public class ArticleViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewName;
    public TextView textViewAuthor;
    public TextView textViewCreatedAt;
    public long articleId;

    public ArticleViewHolder(View itemView) {
        super(itemView);

        textViewName = itemView.findViewById(R.id.textview_article_name);
        textViewAuthor = itemView.findViewById(R.id.textview_article_author);
        textViewCreatedAt = itemView.findViewById(R.id.textview_article_created_at);



        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), ArticleDetailActivity.class);
                intent.putExtra("articleId", articleId);
                v.getContext().startActivity(intent);

            }
        });


    }


}
