package nl.novacollege.ictacademie.amo.android.androidexamples.widget;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import nl.novacollege.ictacademie.amo.android.androidexamples.R;
import nl.novacollege.ictacademie.amo.android.androidexamples.database.sqlite.ToDoListContract;


/**
 * Created by ivo on 26/03/2018.
 */

public class ToDoRecycleViewAdapter extends RecyclerView.Adapter<ToDoViewHolder> {


    private Context context;
    private Cursor cursor;

    public ToDoRecycleViewAdapter(Context context, Cursor cursor) {
        super();
        this.context = context;
        this.cursor = cursor;
    }

    @Override
    public ToDoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        View view = layoutInflater.inflate(R.layout.layout_todo_item, parent, false);
        return new ToDoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoViewHolder holder, int position) {
        // Bind data to cursor if there is data lef
        if (cursor.moveToPosition(position)) {

            String name = cursor.getString(cursor.getColumnIndexOrThrow(ToDoListContract.ToDoListEntry.COLUMN_NAME));
            String description = cursor.getString(cursor.getColumnIndexOrThrow(ToDoListContract.ToDoListEntry.COLUMN_DESCRIPTION));
            long id = cursor.getLong(cursor.getColumnIndexOrThrow(ToDoListContract.ToDoListEntry._ID));

            holder.itemView.setTag(id);
            holder.textViewName.setText(name);
            holder.textViewDescription.setText(description);
        }

    }

    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public void notifyWithNewData(Cursor newCursor) {

        // When cursor is set, we need to close it before we attach a new one
        if (cursor != null) {
            cursor.close();
        }

        // Attach new cursor
        cursor = newCursor;

        // If there is any, notify there is new data
        if (cursor != null) {
            notifyDataSetChanged();
        }

    }


}
