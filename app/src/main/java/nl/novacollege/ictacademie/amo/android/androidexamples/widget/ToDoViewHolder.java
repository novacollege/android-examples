package nl.novacollege.ictacademie.amo.android.androidexamples.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nl.novacollege.ictacademie.amo.android.androidexamples.R;


/**
 * Created by ivo on 26/03/2018.
 */

public class ToDoViewHolder extends RecyclerView.ViewHolder {

    public TextView textViewName;
    public TextView textViewDescription;

    /**
     * Constructor
     * @param itemView
     */
    public ToDoViewHolder(View itemView) {
        super(itemView);

        textViewName = itemView.findViewById(R.id.textview_name);
        textViewDescription = itemView.findViewById(R.id.textview_description);
    }
}